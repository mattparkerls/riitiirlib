@isTest
private class SObjectDescribeTest {
	
	@isTest
	static void NamespacedAttributeMap_implementations(){
		SObjectDescribe.GlobalDescribeMap gdm = SObjectDescribe.getGlobalDescribe();
		Schema.SObjectType accountObjType = gdm.get('AccOunT');
		System.assertEquals(accountObjType, Account.SobjectType);
		System.assertEquals(Schema.getGlobalDescribe().size(), gdm.size());

		SObjectDescribe acccountDescribe = SObjectDescribe.getDescribe(accountObjType);
		SObjectDescribe.FieldsMap fields = acccountDescribe.getFields();
		System.assert( fields.keySet().containsAll(acccountDescribe.getFieldsMap().keySet()) );

		System.assertEquals(fields.get('name'), Account.SObjectType.fields.name); //behavior of FieldsMap is tested in another method
		System.assertEquals(Schema.SObjectType.Account.fields.getMap().size(), fields.size());
	}

	@isTest
	static void FieldsMap(){
		String fakeNamespace = 'test';
		Map<String,Schema.SObjectField> fakeFieldData = new Map<String,Schema.SObjectField>{
			'name__c' => Contact.SObjectType.fields.name, //re-use stndard field types since we can't mock them
			fakeNamespace+'__name__c' => Account.SObjectType.fields.name,
			'createddate' => Contact.SObjectType.fields.CreatedDate
		};
		SObjectDescribe.FieldsMap fields = new SObjectDescribe.FieldsMap(fakeFieldData);
		fields.currentNamespace = fakeNamespace;
		System.assertEquals(true, fields.containsKey('name__c') );
		System.assertEquals(true, fields.containsKey(fakeNamespace+'__name__c') );
		System.assert(fields.get('name__c') === fields.get(fakeNamespace+'__name__c'));

		fields.currentNamespace = 'someOtherNamespace';
		System.assertNotEquals(fields.get('name__c'), fields.get(fakeNamespace+'__name__c'));
	}

	@isTest
	static void GlobalDescribeMap(){
		String fakeNamespace = 'test';
		Map<String,Schema.SObjectType> fakeFieldData = new Map<String,Schema.SObjectType>{
			'name__c' => Contact.SObjectType, //re-use stndard object types since we can't mock them
			fakeNamespace+'__name__c' => Account.SObjectType,
			'createddate' => Lead.SObjectType
		};
		SObjectDescribe.GlobalDescribeMap gdm = new SObjectDescribe.GlobalDescribeMap(fakeFieldData);
		gdm.currentNamespace = fakeNamespace;
		System.assertEquals(true, gdm.containsKey('name__c') );
		System.assertEquals(true, gdm.containsKey(fakeNamespace+'__name__c') );
		System.assert(gdm.get('name__c') === gdm.get(fakeNamespace+'__name__c'));

		gdm.currentNamespace = 'someOtherNamespace';
		System.assertNotEquals(gdm.get('name__c'), gdm.get(fakeNamespace+'__name__c'));
	}
	
	@isTest //Tests all forms of the getDescribe static
	static void getAccountDescribes(){
		SObjectDescribe d = SObjectDescribe.getDescribe('Account');
		SObjectDescribe d2 = SObjectDescribe.getDescribe(Account.SObjectType);
		SObjectDescribe d3 = SObjectDescribe.getDescribe(Schema.SObjectType.Account);
		System.assertEquals('Account', d.getDescribe().getName());
		System.assert( (d === d2 && d2 === d3) ,'All three getDescribe calls should return the same cached instance.');
	}

	@isTest
	static void simpleAccountFieldDescribe(){
		SObjectDescribe d = SObjectDescribe.getDescribe(Account.SObjectType);
		Map<String,Schema.SObjectField> fields;
		for(integer i = 0; i < 10; i++){
			fields = d.getFieldsMap();
		}
		System.assertEquals(false,fields.isEmpty());
	}

	@isTest
	static void simpleAccountFieldSetDescribe(){ 
		SObjectDescribe d = SObjectDescribe.getDescribe(Account.SObjectType);
		Map<String,Schema.FieldSet> fields;
		for(integer i = 0; i < 10; i++){
			fields = d.getFieldSetsMap();
		}
		
		// We need to assert something here... but what?
		//no asserts on result size to avoid a requirement on field sets existing
	}
	
	@isTest
	static void simpleAccountGetNameField(){
        	SObjectDescribe d = SObjectDescribe.getDescribe(Account.SObjectType);
        	Schema.SObjectField nameField = d.getNameField();
        	System.assertEquals('Name', nameField.getDescribe().getName());
	}

	@isTest
	static void flushCache(){
		SObjectDescribe d = SObjectDescribe.getDescribe('Account');
		SObjectDescribe.flushCache();
		SObjectDescribe d2 = SObjectDescribe.getDescribe('Account');
		System.assert(d !== d2, 'Second object should be a fresh instance after a cache flush.' );
	}

	@isTest
	static void rawGlobalDescribeCheck(){
		Map<String,Schema.SObjectType> systemGd = Schema.getGlobalDescribe();
		Map<String,Schema.SObjectType> cachedGd = SObjectDescribe.getRawGlobalDescribe();
		System.assertEquals(systemGd.size(),cachedGd.size());
	}

}
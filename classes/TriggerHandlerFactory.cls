/*
* Cloudreach LTD
* @reference https://github.com/kevinohara80/sfdc-trigger-framework
*/
global virtual class TriggerHandlerFactory {

  // static map of handlername, times run() was invoked
  private static Map<String, LoopCount> loopCountMap;
  private static Set<String> bypassedHandlers;

  static {
    loopCountMap = new Map<String, LoopCount>();
    bypassedHandlers = new Set<String>();
  }
    
  global TriggerHandlerFactory() {

  }

  /***************************************
   * public methods
   ***************************************/

  // main method that will be called during execution
  global void run() {

    if(!validateRun()) return;

    addToLoopCount();

    // dispatch to the correct handler method
    if(Trigger.isBefore && Trigger.isInsert) {
      this.beforeInsert();
    } else if(Trigger.isBefore && Trigger.isUpdate) {
      this.beforeUpdate();
    } else if(Trigger.isBefore && Trigger.isDelete) {
      this.beforeDelete();
    } else if(Trigger.isAfter && Trigger.isInsert) {
      this.afterInsert();
    } else if(Trigger.isAfter && Trigger.isUpdate) {
      this.afterUpdate();
    } else if(Trigger.isAfter && Trigger.isDelete) {
      this.afterDelete();
    } else if(Trigger.isAfter && Trigger.isUndelete) {
      this.afterUndelete();
    }
    
  }

  global void setMaxLoopCount(Integer max) {
    String handlerName = getHandlerName();
    if(!TriggerHandlerFactory.loopCountMap.containsKey(handlerName)) {
      TriggerHandlerFactory.loopCountMap.put(handlerName, new LoopCount(max));
    } else {
      TriggerHandlerFactory.loopCountMap.get(handlerName).setMax(max);
    }
  }

  global void clearMaxLoopCount() {
    this.setMaxLoopCount(-1);
  }

  global static void bypass(String handlerName) {
    TriggerHandlerFactory.bypassedHandlers.add(handlerName);
  }

  global static void clearBypass(String handlerName) {
    TriggerHandlerFactory.bypassedHandlers.remove(handlerName);
  }

  global static Boolean isBypassed(String handlerName) {
    return TriggerHandlerFactory.bypassedHandlers.contains(handlerName);
  }

  global static void clearAllBypasses() {
    TriggerHandlerFactory.bypassedHandlers.clear();
  }

  /***************************************
   * private methods
   ***************************************/

  private void addToLoopCount() {
    String handlerName = getHandlerName();
    if(TriggerHandlerFactory.loopCountMap.containsKey(handlerName)) {
      Boolean exceeded = TriggerHandlerFactory.loopCountMap.get(handlerName).increment();
      if(exceeded) {
        Integer max = TriggerHandlerFactory.loopCountMap.get(handlerName).max;
        throw new TriggerHandlerException('Maximum loop count of ' + String.valueOf(max) + ' reached in ' + handlerName);
      }
    }
  }

  // make sure this trigger should continue to run
  private Boolean validateRun() {
    if(!Trigger.isExecuting) {
      throw new TriggerHandlerException('Trigger handler called outside of Trigger execution');
    }
    if(TriggerHandlerFactory.bypassedHandlers.contains(getHandlerName())) {
      return false;
    }
    return true;
  }

  private String getHandlerName() {
    return String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));
  }

  /***************************************
   * context methods
   ***************************************/

  // context-specific methods for override
  global virtual void beforeInsert(){}
  global virtual void beforeUpdate(){}
  global virtual void beforeDelete(){}
  global virtual void afterInsert(){}
  global virtual void afterUpdate(){}
  global virtual void afterDelete(){}
  global virtual void afterUndelete(){}

  /***************************************
   * inner classes
   ***************************************/

  // inner class for managing the loop count per handler
  private class LoopCount {
    private Integer max;
    private Integer count;

    public LoopCount() {
      this.max = 5;
      this.count = 0;
    }

    public LoopCount(Integer max) {
      this.max = max;
      this.count = 0;
    }

    public Boolean increment() {
      this.count++;
      return this.exceeded();
    }

    public Boolean exceeded() {
      if(this.max < 0) return false;
      if(this.count > this.max) {
        return true;
      }
      return false;
    }

    public Integer getMax() {
      return this.max;
    }

    public Integer getCount() {
      return this.count;
    }

    public void setMax(Integer max) {
      this.max = max;
    }
  }

  // exception class
  public class TriggerHandlerException extends Exception {}

}
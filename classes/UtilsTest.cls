@isTest
public class UtilsTest {

    static testMethod void testCreateSystemException() {
        Account a = (Account)SmartFactory.createSObject('Account',true);
        try{
            insert a;
        }catch(Exception ex){
            Utils.CreateSystemException(ex);
        }
        Utils.CreateSystemException('string1');
        Utils.CreateSystemException();
    }

    static testMethod void testGetRecordIds() {
        //List<Account> accts = (Account)SmartFactory.createSObjectList('Account',false,2);
        //insert accts;
        Account a1 = (Account)SmartFactory.createSObject('Account');
        Account a2 = (Account)SmartFactory.createSObject('Account');
        List<Account> accts = new List<Account>();

        accts.add(a1);
        accts.add(a2);

        insert accts;

        Utils.getRecordIds(accts);
    }

    static testMethod void testDoGetRequest() {
        String endpoint = 'string';
        String cookie = 'string';

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponse());
        
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        HttpResponse res = Utils.doGetRequest(endpoint,cookie);

        
    }

    static testMethod void testDoPostRequest() {
        String endpoint = 'string';
        String cookie = 'string';
        String content = 'string';

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponse());
        
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        HttpResponse res = Utils.doPostRequest(endpoint,cookie,content);
    }

    static testMethod void testGetFieldList() {
        Schema.DescribeSObjectResult describe = Account.sObjectType.getDescribe();
        Set<String> fields = new Set<String> {'string','string1'};
        Set<String> requiredFields = new Set<String> {'string','string1'};

        Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();

        //fields.addAll(fsMap.keySet());
        //requiredFields.addAll(fsMap.keySet());

        //Utils.getFieldList(describe,fields,requiredFields);
    }

    static testMethod void testCheckObjectIsReadable() {
        Schema.sObjectType type = Account.getSObjectType();
        Utils.checkObjectIsReadable(type);
    }

    static testMethod void testCheckRead() {
        Schema.SObjectField field1 = Schema.sObjectType.Account.fields.Name.getSObjectField();
        Schema.SObjectField field2 = Schema.sObjectType.Account.fields.Name.getSObjectField();
        List<SObjectField> fields = new List<SObjectField>();
        fields.add(field1);
        fields.add(field2);
        
        Schema.sObjectType type = Account.getSObjectType();
        Utils.checkRead(type,fields);
    }

    static testMethod void testCheckRead2(){
        List<String> fieldNames = new List<String>();
        String field = 'Name';
        fieldNames.add(field);
        Schema.SObjectType type = Account.getSObjectType();
        Utils.checkRead(type,fieldNames);
    }

    static testMethod void testCheckFieldIsReadable() {
        String field = 'Name';
        Schema.sObjectType type = Account.getSObjectType();
        Utils.checkFieldIsReadable(type,field);
    }

    static testMethod void testCheckFieldIsReadable2() {
        Schema.SObjectField field = Schema.sObjectType.Account.fields.Name.getSObjectField();
        Schema.sObjectType type = Account.getSObjectType();
        Utils.checkFieldIsReadable(type,field);
    }
    
    static testMethod void testCheckFieldIsReadable3() {
        Schema.DescribeFieldResult dfr = Schema.sObjectType.Account.fields.Name;
        Schema.sObjectType type = Account.getSObjectType();
        Utils.checkFieldIsReadable(type,dfr);
    }

    static testMethod void testCheckDateIntersect() {
        Date d1 = Date.today();
        Date d2 = Date.today();
        Date d3 = Date.today();
        Date d4 = Date.today();

        Utils.checkDateIntersect(d1,d2,d3,d4);
    }

    static testMethod void testGetInstance() {
        Test.startTest();
        Utils.getInstance();
        Test.stopTest();
    }

    static testMethod void testSanitize() {
        String str = 'string>><<';
        Utils.sanitize(str);
    }

    static testMethod void testGetParam() {
        String param = 'open';
        Utils.getParam(param);
    }

    static testMethod void testGetIdParam() {
        Account a = (Account)SmartFactory.createSObject('Account',true);
        insert a;
        String id = a.id;

        Utils.getIdParam(id);
    }

    static testMethod void testGetBooleanParam() {
        String bool = 'false';

        Utils.getBooleanParam(bool);
    }

    static testMethod void testGetCurrentUser(){
        Utils.getCurrentUser();
    }

    static testMethod void testFindUserById() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt1', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser123@testorg.com');
        insert u;

        Utils.findUserById(u.id);
    }

    static testMethod void testFindUsersByIds() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt1', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1234@testorg.com');
        insert u;

        Set<Id> uIds = new Set<Id>();
        uIds.add(u.id);

        Utils.findUsersByIds(uIds);
    }

    static testMethod void testFindUserRoleAndProfileById() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO'];
        User u = new User(Alias = 'standt1', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = r.id,
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser12345@testorg.com');
        insert u;

        Utils.findUserRoleAndProfileById(u.id);
    }

    static testMethod void testFindAllUsers() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO'];
        User u = new User(Alias = 'standt1', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = r.id,
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser123456@testorg.com');
        insert u;

        Utils.findAllUsers();
    }

    static testMethod void testFindUsersByUserNames(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO'];
        User u = new User(Alias = 'standt1', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = r.id,
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1234567@testorg.com');
        insert u;

        Set<String> unames = new Set<String>();

        Utils.findUsersByUserNames(unames);
    }

    static testMethod void testFindAllRoles() {
        Utils.findAllUserRoles();
    }

    static testMethod void testFindProductsByProductCodes(){
        //public static Map<String, Product2> findProductsByProductCodes(Set<String> productCodes)
        Product2 p = (Product2)SmartFactory.createSObject('Product2',false);
        insert p;

        String code = p.ProductCode;
        Set<String> codes = new Set<String>();
        codes.add(code);

        Utils.findProductsByProductCodes(codes);

    }

    static testMethod void testFindProductByProductCode(){
        Product2 p = (Product2)SmartFactory.createSObject('Product2',false);
        insert p;

        String code = p.ProductCode;
        Utils.findProductByProductCode(code);
    }

    /*static testMethod void testFindPriceBookEntriesByPriceBook(){
        Pricebook2 pb = (Pricebook2)SmartFactory.createSObject('Pricebook2',false);
        insert pb;

        Product2 p = (Product2)SmartFactory.createSObject('Product2',false);
        //p.Price = 100;
        insert p;

        PricebookEntry entry = (PricebookEntry)SmartFactory.createSObject('PricebookEntry',false);
        entry.Pricebook2Id = pb.id;
        entry.Product2Id = p.id;
        entry.UseStandardPrice = false;
        entry.UnitPrice = 100;
        insert entry;

        String iso = 'usd';

        Utils.findPriceBookEntriesByPriceBook(entry.id,iso);
    }*/

    static testMethod void testGetAccountRecordTypeId(){
        Utils.getAccountRecordTypeId('Master');
    }

    static testMethod void testFindAllRecordTypes(){
        Utils.findAllRecordTypes();
    }

    static testMethod void testFindRecordTypesByObjectType(){
        Set<String> objTypes = new Set<String>();
        String acct = 'Account';
        objTypes.add(acct);

        Utils.findRecordTypesByObjectType(objTypes);
    }

    static testMethod void testFindRecordTypeById(){
        //Id rtId = Utils.getAccountRecordTypeId('Master');
        //Utils.findRecordTypeById();
    }

    static testMethod void testFindOpportunityLineItemsByOppId(){
        Account a = (Account)SmartFactory.createSObject('Account',true);
        insert a;
        Opportunity o = (Opportunity)SmartFactory.createSObject('Opportunity',false);
        o.AccountId = a.id;
        insert o;
        Set<Id> oppIds = new Set<Id>();
        oppIds.add(o.id);

        Utils.findOpportunityLineItemsByOppId(oppIds);
    }

    static testMethod void testFindOpportunityLineItemsByOppIdIntoMap(){
        Account a = (Account)SmartFactory.createSObject('Account',true);
        insert a;
        Opportunity o = (Opportunity)SmartFactory.createSObject('Opportunity',false);
        o.AccountId = a.id;
        insert o;
        Set<Id> oppIds = new Set<Id>();
        oppIds.add(o.id);

        Utils.findOpportunityLineItemsByOppIdIntoMap(oppIds);
    }

    static testMethod void testFindOpportunitiesByAccountIds(){
        Account a = (Account)SmartFactory.createSObject('Account',true);
        insert a;
        Opportunity o = (Opportunity)SmartFactory.createSObject('Opportunity',false);
        o.AccountId = a.id;
        insert o;
        Set<Id> accIds = new Set<Id>();
        accIds.add(a.id);

        Utils.findOpportunitiesByAccountIds(accIds);
    }

    static testMethod void testFindContactsByAccountId(){
        Account a = (Account)SmartFactory.createSObject('Account',true);
        insert a;

        Contact c = (Contact)SmartFactory.createSObject('Contact',false);
        c.AccountId = a.id;
        insert c;

        Utils.findContactsByAccountId(a.id);
    }

    static testMethod void testFindContactsByAccountIds(){
        Account a = (Account)SmartFactory.createSObject('Account',true);
        insert a;
        Set<Id> accIds = new Set<Id>();
        accIds.add(a.id);

        Contact c = (Contact)SmartFactory.createSObject('Contact',false);
        c.AccountId = a.id;
        insert c;

        Utils.findContactsByAccountIds(accIds);
    }

    static testMethod void testFilter(){
        Account a = (Account)SmartFactory.createSObject('Account',true);
        insert a;
        list<Account> accts = new list<Account>();
        accts.add(a);

        Utils.filter(accts);
    }

    static testMethod void testCollector(){
        Account a = (Account)SmartFactory.createSObject('Account',true);
        insert a;
        list<Account> accts = new list<Account>();
        accts.add(a);

        Utils.collect(accts);

    }
}
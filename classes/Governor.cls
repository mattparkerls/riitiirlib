/*
* Cloudreach LTD
* @author Matt Parker
*/
global class Governor {

    @testVisible
        static Double dangerMin = 0.95;
    @testVisible
        static Double warnMin   = 0.85;

    public Governor() {
        // CONSTRUCTOR
    }

    /**
     * Enumeration of severities for limit
     * usage related messages and system logs.
     * @type  {Enum}
     */
    public Enum Severity {
        Info, Warning, Danger, Error
    }

    /**
     * Enumeration of supported limit types
     * for the Governor class.
     * @type  {Enum}
     */
    global Enum LimitType {
        AggregateQueries, Callouts, CpuTime, DMLRows,
        DMLStatements, FieldDescribes, PicklistDescribes,
        ChildRelationshipDescribes, RecordTypeDescribes,
        FutureCalls, HeapSize, FieldSetDescribes, ScriptStatements,
        Savepoints, SavepointRollbacks, EmailInvocations,
        Queries, QueryRows, QueryLocatorRows, SOSLQueries
    }

    /**
     * Map of enumeration 'Governor.LimitType' to Limits
     * method that returns the integer value of the org
     * limit in effect for the installed salesforce org
     * @type  {Map<Governor.LimitType,Integer>}
     */
    public static Map<Governor.LimitType,Integer> orgLimits = new Map<LimitType,Integer>{
        LimitType.AggregateQueries                      =>  Limits.getLimitAggregateQueries(),
        LimitType.Callouts                                      =>  Limits.getLimitCallouts(),
        LimitType.CpuTime                                       =>  Limits.getLimitCpuTime(),
        LimitType.DMLRows                                           =>  Limits.getLimitDMLRows(),
        LimitType.DMLStatements                             =>  Limits.getLimitDMLStatements(),
        LimitType.Queries                                           =>  Limits.getLimitQueries(),
        LimitType.Savepoints                                =>  Limits.getLimitSavepoints(),
        LimitType.SavepointRollbacks                    =>  Limits.getLimitSavepointRollbacks(),
        LimitType.FutureCalls                               =>  Limits.getLimitFutureCalls(),
        LimitType.HeapSize                                      =>  Limits.getLimitHeapSize(),
        LimitType.EmailInvocations                      =>  Limits.getLimitEmailInvocations(),
        LimitType.QueryRows                                     =>  Limits.getLimitQueryRows(),
        LimitType.QueryLocatorRows                      =>  Limits.getLimitQueryLocatorRows(),
        LimitType.SOSLQueries                               =>  Limits.getLimitSoslQueries()
    };

    /**
     * Map of enumeration 'Governor.LimitType' to Limits
     * method that returns the integer value of the current
     * (in-context) usage of the limit.
     * @type  {Map<Governor.LimitType,Integer>}
     */
    public static Map<Governor.LimitType,Integer> currentLimitUsage = new Map<LimitType,Integer>{
        LimitType.AggregateQueries                      =>  Limits.getAggregateQueries(),
        LimitType.Callouts                                      =>  Limits.getCallouts(),
        LimitType.CpuTime                                       =>  Limits.getCpuTime(),
        LimitType.DMLRows                                           =>  Limits.getDMLRows(),
        LimitType.DMLStatements                             =>  Limits.getDMLStatements(),
        LimitType.Queries                                           =>  Limits.getQueries(),
        LimitType.Savepoints                                =>  Limits.getSavepoints(),
        LimitType.SavepointRollbacks                    =>  Limits.getSavepointRollbacks(),
        LimitType.FutureCalls                               =>  Limits.getFutureCalls(),
        LimitType.HeapSize                                      =>  Limits.getHeapSize(),
        LimitType.EmailInvocations                      =>  Limits.getEmailInvocations(),
        LimitType.QueryRows                                     =>  Limits.getQueryRows(),
        LimitType.QueryLocatorRows                      =>  Limits.getQueryLocatorRows(),
        LimitType.SOSLQueries                               =>  Limits.getSoslQueries()
    };

    public static Map<Governor.LimitType,String> limitTypeLabel = new Map<LimitType,String>{
        LimitType.AggregateQueries                      =>  'Aggregate Queries',
        LimitType.Callouts                                      =>  'API Callouts',
        LimitType.CpuTime                                       =>  'CPU Time',
        LimitType.DMLRows                                           =>  'DML Rows',
        LimitType.DMLStatements                             =>  'DML Statements',
        LimitType.Queries                                           =>  'Queries',
        LimitType.Savepoints                                =>  'Savepoints',
        LimitType.SavepointRollbacks                    =>  'Savepoint Rollbacks',
        LimitType.FutureCalls                               =>  'Future Calls',
        LimitType.HeapSize                                      =>  'Heap Size',
        LimitType.EmailInvocations                      =>  'Email Invocations',
        LimitType.QueryRows                                     =>  'Query Rows',
        LimitType.QueryLocatorRows                      =>  'Query Locator Rows',
        LimitType.SOSLQueries                               =>  'SOSL Queries'
    };

    /**
     * Banner for limit messages
     * @return  {String}
     */
    private static String getLogBanner(Governor.LimitType governorLimitType) {
        return '*** Governor Limits ' + limitTypeLabel.get(governorLimitType) + ' ***\n\n';
    }

    /**
     * Footer for limit messages
     * @return  {String}
     */
    private static String getLogFooter() {
        return '\n\n*** [END of Governor Limits Warning] ***';
    }

    static Governor.Severity getSeverity(Governor.LimitType governorLimitType) {
        Double severityRatio = Double.valueOf(currentLimitUsage.get(governorLimitType)) / Double.valueOf(orgLimits.get(governorLimitType));
        if (severityRatio == 1)
            return Governor.Severity.Error;
        else if (severityRatio >= dangerMin)
            return Governor.Severity.Danger;
        else if (severityRatio >= warnMin)
            return Governor.Severity.Warning;
        return Governor.Severity.Info;
    }

    global static Boolean isUnderLimit(Governor.LimitType governorLimitType) {
        Integer orgLimit = orgLimits.get(governorLimitType);
        Integer currentUsage = currentLimitUsage.get(governorLimitType);
        Integer bandwidth = orgLimit - currentUsage;
        Boolean underLimit = bandwidth >= 1;
        Double thresholdLevel = Double.valueOf(currentUsage) / Double.valueOf(orgLimit);
        Boolean warn = (Double.valueOf(currentUsage) / Double.valueOf(orgLimit)) >= warnMin;
        Boolean danger = (Double.valueOf(currentUsage) / Double.valueOf(orgLimit)) >= dangerMin;
        Boolean writeMessage = (warn || danger);
        if (writeMessage) {
            String msgData = getLogBanner(governorLimitType);
            msgData += 'Organization Limit for '+limitTypeLabel.get(governorLimitType)+': ' + orgLimit;
            msgData += '\n';
            msgData += 'Current Usage for '+limitTypeLabel.get(governorLimitType)+': ' + currentUsage;
            msgData += '\n';
            msgData += 'Current Limit Bandwidth for '+limitTypeLabel.get(governorLimitType)+': ' + bandwidth;
            msgData += '\n';
            msgData += getLogFooter();
            System.Debug(msgData + 'Governor Limit ' + getSeverity(governorLimitType).name());
            Utils.CreateSystemException(msgData + 'Governor Limit ' + getSeverity(governorLimitType).name());
        }
        return underLimit;
    }

}
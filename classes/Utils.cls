/*
* Riitiir, LLC
* @author Matt Parker
*/
global class Utils {

    global static String ns = 'Riitiirlib';
    global static String nsprefix = 'Riitiirlib__';
    global static String nsp = nsprefix;

    /**
    * @throws UtilsException
    */
    global class UtilsException extends Exception { }
    
    /**
    * Create System Logs instead of debug logs
    * @param ex the exception
    */
    global static void CreateSystemException(Exception ex){
        System_Exception__c e = new System_Exception__c();
        e.Exception_Message__c = ex.getMessage();
        if(ex.getStackTraceString().length() < 255)
        e.Stack_Trace__c = ex.getStackTraceString();
        if(ex.getStackTraceString().length() >= 255)
        e.Stack_Trace_Long__c = ex.getStackTraceString();
        e.Exception_Type__c = ex.getTypeName(); 
        insert e;
    }

    /**
    * Create System Logs instead of debug logs
    * debugging WITH message
    * @param debugMsg the debug message
    */
    global static void CreateSystemException(String debugMsg){
        System_Exception__c ex = new System_Exception__c();
        ex.isDebug__c = true;
        ex.Debug_Message__c = debugMsg;
        insert ex;
    }

    /**
    * Create System Logs instead of debug logs
    * debugging WITHOUT message
    */
    global static void CreateSystemException(){
        System_Exception__c ex = new System_Exception__c();
        ex.isDebug__c = true;
        ex.Debug_Message__c = 'DEBUG >>> DEBUG >>> DEBUG >>>';
        insert ex;
    }

    /*
    * Utility to return a Set of Ids for a given list
    * of SObjects
    */

    global static Set<Id> getRecordIds(List<SObject> records) {
        Set<Id> recordIds = new Set<Id>();
        for (SObject o : records)
            recordIds.add(Id.valueOf(String.valueOf(o.get('id'))));
        return recordIds;
    }

   /**
    * Utility method to make an HTTP request.
    * @param endpoint the request endpoint.
    * @param method the request method.
    * @param cookie the request cookie.
    * @return the http response.
    */
    global static HttpResponse doGetRequest(String endpoint, String cookie)
    {
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endpoint);
            req.setMethod('GET');
            req.setHeader('Cookie', cookie == null ? '' : cookie);
            if (Test.isRunningTest())
            {
                return new HttpResponse();
            }
            else
                return (new Http()).send(req);
                
    }

   /**
    * Utility method to make an HTTP POST request.
    * @param endpoint the request endpoint.
    * @param method the request method.
    * @param cookie the request cookie.
    * @param content the post content.
    * @return the http response.
    */
    global static HttpResponse doPostRequest(String endpoint, String cookie, String content){

        HttpRequest req = new HttpRequest(); //instantiate request
        req.setEndpoint(endpoint);
        req.setHeader('Content-Type',  'application/json; charset=utf-8'); //Or just cookie var
        req.setBody(content); 
        req.setMethod('POST');

        if(Test.isRunningTest()){
            return new HttpResponse();
        }
        else{
            return (new Http().send(req));
        }
    }

    /**
     * Get the list of fields for the Supplied object type and field sets
     * 
     * Arguments:   Schema.DescribeSObjectResult    -   obect describe. Eg. Schema.SObjectType.Account
     *              Set<String> -   Array of field set names
     *              Set<String> -   Mandatory fields
     *
     * Returns:     String  -   comma seperated list of fields.
     */
    /*global static String getFieldList(Schema.DescribeSObjectResult describe, Set<String> fieldSets, Set<String> mandatoryFields)
    {
        // Put the field set data in a map
        Map<String, Schema.FieldSet> f = describe.fieldSets.getMap();

        Set<String> allFields = mandatoryFields;

        // loop throught the supplied field sets merging them with the mandatory fields
        for (String fsName : fieldSets)
        {
            Schema.FieldSet fs = f.get(fsName);
            
            // loop through the fields and add them to the list
            for(Schema.FieldSetMember fm : fs.getFields())
            {
                allFields.add(fm.getFieldPath());
            }
        }

        String myFields = '';

        // loop through all the fields
        for (String fName : allFields)
        {
            myFields += fName + ',';
        }
        
        // remove the trailing comma
        if (myFields.endsWith(','))
        {
            myFields = myFields.substring(0, myFields.length() -1);
        }
                
        return myFields;
    } */      


    /**
     * Checks read CRUD for the specified object type.
     * @exception CrudException if the running uder does not have read rights to the {@code objType} SObject.
    **/
    global static void checkObjectIsReadable(SObjectType objType)
    {
        if (!objType.getDescribe().isAccessible())
            throw new UtilsException();
    }

    /**
     * Checks both read FLS and CRUD for the specified object type and fields.
     * @exception FlsException if the running user does not have read rights to any fields in {@code fieldNames}.
     * @exception CrudException if the running user does not have read rights to {@code objType}
     **/
    global static void checkRead(SObjectType objType, List<String> fieldNames)
    {
        checkObjectIsReadable(objType);
        for (String fieldName : fieldNames)
        {
            checkFieldIsReadable(objType, fieldName);
        }
    }
    
    /**
     * Identical to {@link #checkRead(SObjectType,List<String>)}, except with SObjectField instead of String field references.
     * @exception FlsException if the running user does not have read rights to any fields in {@code fieldTokens}.
     * @exception CrudException if the running user does not have read rights to {@code objType}
     **/
    global static void checkRead(SObjectType objType, List<SObjectField> fieldTokens)
    {
        checkObjectIsReadable(objType);
        for (SObjectField fieldToken : fieldTokens)
        {
            checkFieldIsReadable(objType, fieldToken);
        }
    }
    
    /**
     * Checks read field level security only (no CRUD) for the specified fields on {@code objType}
     * @exception FlsException if the running user does not have read rights to the {@code fieldName} field.
    **/
    global static void checkFieldIsReadable(SObjectType objType, String fieldName)
    {
        checkFieldIsReadable(objType, SObjectDescribe.getDescribe(objType).getField(fieldName));
    }

    /**
     * Identical to {@link #checkFieldIsReadable(SObjectType,String)}, except with SObjectField instead of String field reference.
     * @exception FlsException if the running user does not have read rights to the {@code fieldName} field.
    **/
    global static void checkFieldIsReadable(SObjectType objType, SObjectField fieldToken)
    {
        checkFieldIsReadable(objType, fieldToken.getDescribe());
    }

    /**
     * Identical to {@link #checkFieldIsReadable(SObjectType,String)}, except with DescribeFieldResult instead of String field reference.
     * @exception FlsException if the running user does not have read rights to the {@code fieldName} field.
    **/
    global static void checkFieldIsReadable(SObjectType objType, DescribeFieldResult fieldDescribe)
    {
        if (!fieldDescribe.isAccessible())
            throw new UtilsException();
    }
    
    
    /**
     * Compare 2 date ranges and return true if there is an intersect between them
     *
     * Arguments:   Date    -   Date range 1 - from date
     *              Date    -   Date range 1 - to date
     *              Date    -   Date range 2 - from date
     *              Date    -   Date range 2 - to data
     *
     * Returns:     true if there is an intersect; false if no intersect
     */
    global static Boolean checkDateIntersect(Date date1From, Date date1To, Date date2From, Date date2To)
    {
        // Determine the greater of the 2 from dates
        Date fromDate = date1From > date2From ? date1From : date2From;
        
        // Determine the lesser of the 2 to dates
        Date toDate = date1To < date2To ? date1To : date2To;
        
        // Calculate the number of days between the start and end date
        Integer daysBetween = fromDate.daysBetween(toDate);
        
        // if the days between is > 0, we have an intersect
        if (daysBetween > 0)
        {
            return true;
        }
    
        return false;
    }

    /**
     * Returns the Salesforce instance for the current org:
     * na14, na11, cs3, etc...
     *
     * @return String
     */
    global static String getInstance() {
        if (System.Test.isRunningTest()) {
            return 'na11';
        }
        String host = ApexPages.currentPage().getHeaders().get('Host');
        String[] splitHost = host.split('\\.',3);
        if (host.contains('visual'))
            return splitHost[1];
        else
            return splitHost[0];
    }

    /**
     * Santize XSS-vulnerabile HTML reserved characters
     * while allowing for common param values (e.g., &)
     */
    global static String sanitize(String s) {
        if (s == null) {
            return s;
        }
        String es = s.replace('<','&lt;');
        es = es.replace('>','&gt;');
        return es;
    }


    /**
     * Returns string value of the requested
     * page parameter.
     *
     * @param param Name of the page parameter.
     * @return String
     */
    global static String getParam(String param) {
        String s = ApexPages.CurrentPage().getParameters().get(param);
        s = sanitize(s);
        return s != null ? s : '';
    }

    /**
     * Returns id value of the requested
     * page parameter.
     *
     * @param param Name of the page parameter.
     * @return Id
     */
    global static Id getIdParam(String param) {
        String s = getParam(param);
        return s == '' ? null : id.valueof(s);
    }

    /**
     * Returns boolean value of the requested
     * page parameter.
     *
     * @param  param Name of the page parameter
     * @return Boolean
     */
    global static Boolean getBooleanParam(String param) {
        String s = getParam(param);
        return ((s == '') || (s == 'false')) ? false : true;
    }

    /**
     * Returns the running User
     *
     * @param  null
     * @return User
     */
    global static User getCurrentUser(){
        return [SELECT u.Id, u.UserRole.Name, u.UserRoleId, u.Profile.Name, u.ProfileId, u.Name FROM User u WHERE u.Id = :UserInfo.getUserId()];
    }

    /**
     * Find User by it's user Id. This allows fields not available
     * by either the UserInfo object or $User merge field to be retrieved.
     * 
     * Arguments:   Id  -   User Id to search for
     *
     * Returns:     User    -   Returns a user object or null
     */
    global static User findUserById(Id userId)
    {
        return [Select u.Id, u.Name From User u where u.id = :userId];
    }
    
    /**
     * Find User and Profile by it user Id. This allows fields not available
     * by either the UserInfo object or $User merge field to be retrieved.
     * 
     * Arguments:   Id  -   User Id to search for
     *
     * Returns:     User    -   Returns a deep loaded user object
     */
    global static User findUserRoleAndProfileById(Id userId)
    {
        return [Select u.UserRole.Name, u.UserRoleId, u.Profile.Name, u.ProfileId, u.Name From User u where u.id = :userId];
    }

    /**
     * Find Users by Ids
     *
     * Arguments:   Set<Id> -   set of user ids
     *
     * Returns:     Map<Id, User>   -   Map of users
     */
    global static Map<Id, User> findUsersByIds(Set<Id> userIds)
    {
        return new Map<Id, User>([Select u.Name, u.IsActive, u.Id, u.UserRoleId, u.ManagerId
                                  From User u
                                  Where u.Id in :userIds]);
    }

    /**
     * Find All Users
     *
     * Arguments:   None
     *
     * Returns:     Map<Id, User>   -   Map of users
     */
    global static Map<Id, User> findAllUsers()
    {
        return new Map<Id, User>([Select u.Name, u.IsActive, u.Id, u.UserRoleId, u.ManagerId
                                  From User u
                                  Where u.IsActive = true]);
    }
    
    /**
     * Find Users by their User Names.
     * 
     * Arguments:   Set<String> -   User names to search for
     *
     * Returns:     Map<String, User>   -   Returns a Map of Users
     */
    global static Map<String, User> findUsersByUserNames(Set<String> userNames)
    {
        Map<String, User> userMap = new Map<String, User>();
        
        for (User usr : [Select u.Id, u.Name, u.Username
                            From User u 
                            Where u.Username != null and u.Username in :userNames])
        {
            userMap.put(usr.Username, usr);
        }
        
        return userMap;
    }

    /**
     * Find All Roles in the Hierarchy
     *
     * Arguments:   n/a
     *
     * Returns:     Map of UserRole Id to Roles
     */
     global static Map<Id, UserRole> findAllUserRoles()
     {
        return new Map<Id, UserRole>([Select u.ParentRoleId, u.Name, u.Id From UserRole u]);
     }

    /**
     * Find Products by Product Code.
     *
     * Arguments:   Set<String>             Set of Product Codes to search for
     *
     * Returns:     Map<String, Product2>   Map of Product Codes to Product2
     */
    global static Map<String, Product2> findProductsByProductCodes(Set<String> productCodes)
    {
        // Create the map to populate
        Map<String, Product2> productMap = new Map<String, Product2>();
        
        // query the product record and store the results in the map
        for (Product2[] products : [Select p.Id, p.Name, p.ProductCode, p.Description
                                    from Product2 p 
                                    where p.ProductCode in :productCodes and 
                                        p.ProductCode != null])
        {
            for (Product2 product : products)
            {
                productMap.put(product.ProductCode, product);
            }
        }                                   
        
        return productMap;
    }
    /**
     * Find a Product by Product Code.
     *
     * Arguments:   String                  Product Codes to search for
     *
     * Returns:     Product2                Product found, null is not 
     */
    global static Product2 findProductByProductCode(String productCode)
    {
        Product2 foundProduct;
        
        // query the product record 
        foundProduct = [Select p.Id, p.Name, p.ProductCode, p.Description
                        from Product2 p 
                        where p.ProductCode = :productCode];
    
        return foundProduct;
    }

    /**
     * Find Pricebook Entries by Pricebook and Currency code.
     * 
     * Arguments:   Id  -   Pricebook id
     *
     * Returns:     List<PriceBookEntry>    -   Returns a list of pricebook entries
     */
    global static List<PriceBookEntry> findPriceBookEntriesByPriceBook(Id priceBookId, String currencyIsoCode)
    {
        
        return [select id, Name, unitprice, Product2Id, Product2.Family, Product2.IsActive,
                    Product2.ProductCode, Product2.Name, IsActive
                from PricebookEntry
                where Product2.IsActive = true and 
                    isActive = true and Pricebook2Id = :priceBookId];
    }

    @deprecated
    //This method is deprecated, use getRecordTypeId instead
    global static Id getAccountRecordTypeId(String recordtypeName)
    {
        // Get the record type associated with supplied name
        return Account.SObjectType.getDescribe().getRecordTypeInfosByName().get(recordtypeName).getRecordTypeId();
    }
    
    /**
     * Return Record Type Id from a Name and SObject API name 
     *
     * Arguments:   String  RecordType Name
     *              String  sObject API Name
     *
     * Returns:     Id
     */
    global static Id getRecordTypeId(String recordtypeName, String objectapiname)
    {
        // Get the sObjectType
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectapiname);

        // Get the record type associated with supplied name
        return targetType.getDescribe().getRecordTypeInfosByName().get(recordtypeName).getRecordTypeId();
    }

    /**
     * Find All Record Types
     *
     * Arguments:   Set<Id>     Set of Ids
     *
     * Returns:     Map<Id, RecordType>
     */
    global static Map<Id, RecordType> findAllRecordTypes()
    {
        return new Map<Id, RecordType>([Select r.Name, r.Id, r.DeveloperName From RecordType r]);
    }

    /**
     * Find Record Types by ObjectType
     *
     * Arguments:   Set<Id>     Set of Ids
     *              Set<String> Set of ObjectType Names
     *
     * Returns:     Map<Id, RecordType>
     */
    global static Map<Id, RecordType> findRecordTypesByObjectType(Set<String> objectTypes)
    {
        return new Map<Id, RecordType>([Select r.Name, r.Id, r.DeveloperName
                                        From RecordType r
                                        Where r.SobjectType in :objectTypes]);
    }
    /**
     * Find Record Types by id
     *
     * Arguments:   Id          Id to search for
     *
     * Returns:     RecordType
     */
    global static RecordType findRecordTypeById(Id anId)
    {
        return ([select r.Name, r.Id, r.DeveloperName
                    from RecordType r 
                    where r.Id = :anId]);
    }

    /**
     * Determines whether the Account is a Person Account
     *
     * Arguments:   Id          Account Id
     *
     * Returns:     Boolean
     */
    /*global static Boolean isPersonAccount(Id acctId)
    {
        RecordType rtId = getAccountRecordTypeId('Person Account');
        Account a = [SELECT id,recordtypeid
                        FROM Account 
                        WHERE id=:acctId];
        if(a.recordtypeid==rtId){
            return true;
        }else{
            return false;
        }
    }*/

    /**
     * Find Opprotunity Pproducts by Opportunity Ids
     *
     * Arguments:   Set<Id>     -   Set of Opportunity Ids
     *
     * Returns:     List<OpportunityLineItem>   -   List of Opportunity Line Items
     */
    global static OpportunityLineItem[] findOpportunityLineItemsByOppId(Set<Id> opportunityIds)
    {
        return [Select o.TotalPrice, o.Quantity, o.PricebookEntry.Product2Id, o.PricebookEntryId, 
                    o.Opportunity.Id, o.OpportunityId, o.UnitPrice, o.ListPrice
                From OpportunityLineItem o
                Where o.OpportunityId in :opportunityIds];
    }

    /**
     * Find Opprotunity Products by Opportunity Ids and return them in a Map
     *
     * Arguments:   Set<Id>     -   Set of Opportunity Ids
     *
     * Returns:     Map<Id, OpportunityLineItem[]>  -   Map of Opportunity Line Items keyed by Opportunity Id
     */
    global static Map<Id, OpportunityLineItem[]> findOpportunityLineItemsByOppIdIntoMap(Set<Id> opportunityIds)
    {
        Map<Id, OpportunityLineItem[]> oppMap = new Map<Id, OpportunityLineItem[]>();
        
        for (OpportunityLineItem itm : [Select o.TotalPrice, o.Quantity, o.PricebookEntry.Product2Id, o.PricebookEntryId, 
                    o.OpportunityId, o.UnitPrice, o.ListPrice
                From OpportunityLineItem o
                Where o.OpportunityId in :opportunityIds])
        {
            // Check the map the Opportunity Id
            List<OpportunityLineItem> items = oppMap.get(itm.OpportunityId);
            
            if (items == null)
            {
                items = new List<OpportunityLineItem>();
                oppMap.put(itm.OpportunityId, items);
            }

            items.add(itm);         
        }
        
        return oppMap;
    }

    /**
     * Find Opportunities by Account Ids
     *
     * Arguments:   Set<Id>     - Set of Account Ids
     *
     * Returns:     Map<Id, Opportunity[]>  -   Map of Opportunities keyed on Account Id
     */
    global static Map<Id, List<Opportunity>> findOpportunitiesByAccountIds(Set<Id> accountIds)
    {
        Map<Id, List<Opportunity>> oppMap = new Map<Id, List<Opportunity>>();
        
        for (Opportunity opp : [Select Id, Name, OwnerId, AccountId from Opportunity where AccountId in :accountIds])
        {
            // Get the list from the map
            List<Opportunity> opps = oppMap.get(opp.AccountId);
            
            // if it's null create it and add it to the map
            if (opps == null)
            {
                opps = new List<Opportunity>();
                oppMap.put(opp.AccountId, opps);
            }
            
            opps.add(opp);
        }   
        
        return oppMap;      
    }

    /**
     * Find Contacts by Account Id.
     * 
     * Arguments:   Id  -   Account Id to search for
     *
     * Returns:     Contact[]   -   Returns an array of Contact
     */
    global static Contact[] findContactsByAccountId(Id accountId)
    {
        return [Select c.Id, c.Name, c.AccountId From Contact c
                where c.AccountId = :accountId];
    }

    /**
     * Find Contacts by Account Ids.
     * 
     * Arguments:   Set<Id>     -   Account Ids to search for
     *
     * Returns:     Map<Id, List<Contact>>  -   Returns a Map of Contacts keyed by Account
     */
    global static Map<Id, List<Contact>> findContactsByAccountIds(Set<Id> accountIds)
    {
        // Create a new Map
        Map<Id, List<Contact>> contactMap = new Map<Id, List<Contact>>();
        Id lastId;
        List<Contact> contactList;
                    
        for (Contact cont : [Select c.Salutation, c.Phone, c.Name, c.LastName, c.FirstName, c.Email, c.AccountId From Contact c
                                where c.AccountId in :accountIds order by c.AccountId])
        {
            // if the account Id has changed create a new list and add it to the map
            if (cont.AccountId != lastId)
            {
                contactList = new List<Contact>();
                contactMap.put(cont.AccountId, contactList);
                lastId = cont.AccountId;
            }
            
            contactList.add(cont);          
        }
        
        return contactMap;
    }

    /**
     * Returns a filter that will allow to filter the given source list of SObjects
     * in different ways.
     *
     * Arguments:   List<SObject>     -   The source list of sobjects to  filter
     *
     * Returns:     Filter  -   A filter object that allows to define the filters to apply.
     */
    global static Filter filter(List<SObject> source) {
        return Filter.source(source);
    }

    /**
     * Returns a collector that will allow to group the elements of the given collection
     * into a Map.
     *
     * Arguments:   List<SObject>     -   The source list of sobjects to use as a base
     *
     * Returns:     Collector  -   A Collector object that allows to define how to collect 
     * elements of the list
     */
    global static Collector collect(List<SObject> source) {
        return Collector.collect(source);
    }

}
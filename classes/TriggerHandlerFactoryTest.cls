@isTest
public class TriggerHandlerFactoryTest {

	public class TriggerHandlerFactoryWithoutOverride extends TriggerHandlerFactory {}

	public class TriggerHandlerFactoryWithOverride extends TriggerHandlerFactory {

	    public override void beforeInsert() {} 
	  
	    public override void afterInsert() {}
	    
	    public override void beforeUpdate() {}

	    public override void afterUpdate() {}
	    
	    public override void beforeDelete() {}
	    
	    public override void afterDelete() {}

	    public override void afterUndelete() {}
	}

	static testMethod void testTriggerHandlerFactory() {
		Test.startTest();
		TriggerHandlerFactoryWithoutOverride handler = new TriggerHandlerFactoryWithoutOverride();
		handler.beforeInsert();
		handler.afterInsert();
		handler.beforeUpdate();
		handler.afterUpdate();
		handler.beforeDelete();
		handler.afterDelete();
		handler.afterUndelete();
		Test.stopTest();
	}

	static testMethod void testTriggerHandlerWithOverride() {
		Test.startTest();
			TriggerHandlerFactoryWithOverride handler = new TriggerHandlerFactoryWithOverride();
			handler.setMaxLoopCount(1);
			handler.clearMaxLoopCount();
			Account a1 = (Account)SmartFactory.createSObject('Account',false);
			TriggerHandlerFactory.bypass('TriggerHandlerFactoryWithOverride');
			boolean isBypassed = TriggerHandlerFactory.isBypassed('TriggerHandlerFactoryWithOverride');
			system.assertEquals(true,isBypassed);
			insert a1;
			TriggerHandlerFactory.clearBypass('TriggerHandlerFactoryWithOverride');
			TriggerHandlerFactory.clearAllBypasses();
		Test.stopTest();
	}
	static testMethod void testTriggerHandlerWithOverrideDelete() {
		Test.startTest();
			TriggerHandlerFactoryWithOverride handler = new TriggerHandlerFactoryWithOverride();
			handler.setMaxLoopCount(1);
			handler.clearMaxLoopCount();
			Account a1 = (Account)SmartFactory.createSObject('Account',false);
			TriggerHandlerFactory.bypass('TriggerHandlerFactoryWithOverride');
			boolean isBypassed = TriggerHandlerFactory.isBypassed('TriggerHandlerFactoryWithOverride');
			system.assertEquals(true,isBypassed);
			insert a1;
			TriggerHandlerFactory.clearAllBypasses();
			//system.assertEquals(false,isBypassed);
			delete a1;
			undelete a1;
			
		Test.stopTest();
	}
	static testMethod void testTriggerHandlerWithOverrideUpdate() {
		Test.startTest();
			TriggerHandlerFactoryWithOverride handler = new TriggerHandlerFactoryWithOverride();
			handler.setMaxLoopCount(1);
			handler.clearMaxLoopCount();
			Account a1 = (Account)SmartFactory.createSObject('Account',false);
			TriggerHandlerFactory.bypass('TriggerHandlerFactoryWithOverride');
			boolean isBypassed = TriggerHandlerFactory.isBypassed('TriggerHandlerFactoryWithOverride');
			system.assertEquals(true,isBypassed);
			insert a1;
			TriggerHandlerFactory.clearBypass('TriggerHandlerFactoryWithOverride');
			//system.assertEquals(false,isBypassed);
			a1.name = 'Test Account';
			update a1;
		Test.stopTest();
	}
}
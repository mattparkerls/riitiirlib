global class Filter {
	private List<SObject> objects;
	private List<Condition> conditions;

	private static Map<ConditionType, ConditionEvaluationStrategy> evaluationStrategy 
			= new Map<ConditionType, ConditionEvaluationStrategy>();

	{
		evaluationStrategy.put(ConditionType.EQUALS, new EqualsEvaluationStrategy());
		evaluationStrategy.put(ConditionType.STARTS_WITH, new StartsWithEvaluationStrategy());
		evaluationStrategy.put(ConditionType.ENDS_WITH, new EndsWithEvaluationStrategy());
	}


	private Filter(List<SObject> objects, List<Condition> conditions) {
		this.objects = objects;
		this.conditions = conditions;
	}

 	global static Filter source(List<SObject> objects) {
 		return new Filter(objects, new List<Condition>());
 	}

 	global Filter removeEquals(SObjectField field, Object value) {
 		this.conditions.add(new Condition(field, value, ConditionType.EQUALS));
 		return this;
 	}

 	global Filter removeStartsWith(SObjectField field, Object value) {
 		this.conditions.add(new Condition(field, value, ConditionType.STARTS_WITH));
 		return this;
 	}

  	global Filter removeEndsWith(SObjectField field, Object value) {
 		this.conditions.add(new Condition(field, value, ConditionType.ENDS_WITH));
 		return this;
 	}

 	global Filter removeNulls(SObjectField field) {
 		this.conditions.add(new Condition(field, null, ConditionType.EQUALS));
 		return this;
 	}
 	
 	global List<SObject> run() {
 		List<SObject> result = new List<SObject>();
 		for (SObject obj : objects) {
 			if (notFiltered(obj, conditions))
 				result.add(obj);
 		}
 		return result;
 	}

 	private Boolean notFiltered(SObject obj, List<Condition> conditions) {
 		for (Condition cond : conditions) {
 			if (cond.eval(obj))
 				return false;
 		}

 		return true;
 	}

	private class Condition {
		private SObjectField field;
		private Object value;
		private ConditionType conditionType;


		private Condition(SObjectField field, Object value, ConditionType conditionType) {
			this.field = field;
			this.value = value;
			this.conditionType = conditionType;
		}

		private Boolean eval(Sobject anObject) {
			Object objValue = anObject.get(field);
			return evaluationStrategy.get(conditionType).eval(objValue, value);
		}
	}

	private enum ConditionType {
		EQUALS, STARTS_WITH, ENDS_WITH
	}

	private interface ConditionEvaluationStrategy {
		Boolean eval(Object value1, Object value2);
	}

	private class EqualsEvaluationStrategy implements ConditionEvaluationStrategy {
		public Boolean eval(Object value1, Object value2) {
			return  value1 == value2;
		}
	}

	private class StartsWithEvaluationStrategy implements ConditionEvaluationStrategy {
		public Boolean eval(Object value1, Object value2) {
			if (value1 == null)
				return false;

			return  ((String) value1).startsWith((String)value2);
		}
	}
	
	private class EndsWithEvaluationStrategy implements ConditionEvaluationStrategy {
		public Boolean eval(Object value1, Object value2) {
			if (value1 == null)
				return false;

			return  ((String) value1).endsWith((String)value2);
		}
	}
	
}
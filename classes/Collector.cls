global class Collector {
	private List<SObject> objects;
	private SObjectField field;
	

	class CollectorException extends Exception {}


	private Collector(List<SObject> objects, SObjectField field) {
		this.objects = objects;	
		this.field = field;
	}
	

	global static Collector collect(List<SObject> objects) {
		if (objects == null)
			throw new CollectorException('Collection can not be empty');
			
		return new Collector(objects, null);
	}

	global Collector byField(SObjectField field) {
		if (this.field != null)
			throw new CollectorException('Only supported collecting by only one field for now');

		this.field = field;
		return this;
	}

	global Map<Object, Set<SObject>> withDuplicates() {
		Map<Object, Set<SObject>> result = new Map<Object, Set<SObject>>();
		for (SObject obj : objects) {
			Set<SObject> resultList = result.get(obj.get(field));
			if (resultList == null) {
				resultList = new Set<SObject>();
				result.put(obj.get(field), resultList);			
			}

			resultList.add(obj);
		}

		return result;
	}

	global Map<Object, SObject> noDuplicates() {
		Map<Object, SObject> result = new Map<Object, SObject>();
		for (SObject obj : objects) {
			result.put(obj.get(field), obj);
		}

		return result;
	}

}
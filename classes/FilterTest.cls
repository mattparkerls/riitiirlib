@isTest
public class FilterTest {

	public static testMethod void filterEquals() {
		List<Opportunity> opportunities = new List<Opportunity>();

		opportunities.add(new Opportunity(name='NAME1'));
		opportunities.add(new Opportunity(name='NAME2'));
		opportunities.add(new Opportunity(name='NAME3'));

		List<SObject> filteredOpps = Filter.source(opportunities).removeEquals(Opportunity.name, 'NAME1').run();

		System.assert(filteredOpps.size() == 2);
		System.assert(((Opportunity) filteredOpps.get(0)).name != 'NAME1');
		System.assert(((Opportunity) filteredOpps.get(1)).name != 'NAME1');

	}

	public static testMethod void filterEqualsMany() {
		List<Opportunity> opportunities = new List<Opportunity>();

		opportunities.add(new Opportunity(name='NAME1'));
		opportunities.add(new Opportunity(name='NAME1'));
		opportunities.add(new Opportunity(name='NAME1'));

		List<SObject> filteredOpps = Filter.source(opportunities).removeEquals(Opportunity.name, 'NAME1').run();

		System.assert(filteredOpps.size() == 0);

	}

	public static testMethod void filterStartsWith() {
		List<Opportunity> opportunities = new List<Opportunity>();

		opportunities.add(new Opportunity(name='NAME1'));
		opportunities.add(new Opportunity(name='NAME2'));
		opportunities.add(new Opportunity(name='NAME3'));

		List<SObject> filteredOpps = Filter.source(opportunities).removeStartsWith(Opportunity.name, 'NAME').run();

		System.assert(filteredOpps.size() == 0);

	}

	public static testMethod void filterEndsWith() {
		List<Opportunity> opportunities = new List<Opportunity>();

		opportunities.add(new Opportunity(name='NAME1'));
		opportunities.add(new Opportunity(name='NAME2'));
		opportunities.add(new Opportunity(name='NAME3'));

		List<SObject> filteredOpps = Filter.source(opportunities).removeEndsWith(Opportunity.name, '1').run();

		System.assert(filteredOpps.size() == 2);
		System.assert(((Opportunity) filteredOpps.get(0)).name != 'NAME1');
		System.assert(((Opportunity) filteredOpps.get(1)).name != 'NAME1');
	}

	public static testMethod void filterNulls() {
		List<Opportunity> opportunities = new List<Opportunity>();

		opportunities.add(new Opportunity(name=null));
		opportunities.add(new Opportunity(name='NAME2'));
		opportunities.add(new Opportunity(name='NAME3'));

		List<SObject> filteredOpps = Filter.source(opportunities).removeNulls(Opportunity.name).run();

		System.assert(filteredOpps.size() == 2);
		System.assert(((Opportunity) filteredOpps.get(0)).name == 'NAME2');
		System.assert(((Opportunity) filteredOpps.get(1)).name == 'NAME3');
	}

	public static testMethod void filterMixedConditions() {
		List<Opportunity> opportunities = new List<Opportunity>();

		opportunities.add(new Opportunity(name='NAME1'));
		opportunities.add(new Opportunity(name='ANAME2'));
		opportunities.add(new Opportunity(name='NAME3'));

		List<SObject> filteredOpps = Filter.source(opportunities)
			.removeStartsWith(Opportunity.name, 'ANAM')
			.removeEquals(Opportunity.name, 'NAME1')
			.run();


		System.assert(filteredOpps.size() == 1);
		System.assert(((Opportunity) filteredOpps.get(0)).name == 'NAME3');
	}
}
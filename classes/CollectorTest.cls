@isTest
public class CollectorTest {

	public static testMethod void testNoDuplicates() {
		List<Opportunity> opportunities = new List<Opportunity>();

		opportunities.add(new Opportunity(name='NAME1'));
		opportunities.add(new Opportunity(name='NAME2'));
		opportunities.add(new Opportunity(name='NAME3'));

		Map<Object, SObject> oppsByName = Collector.collect(opportunities).byField(Opportunity.name).noDuplicates();

		System.assertEquals(opportunities.get(0), oppsByName.get('NAME1'));
		System.assertEquals(opportunities.get(1), oppsByName.get('NAME2'));
		System.assertEquals(opportunities.get(2), oppsByName.get('NAME3'));

	}

	public static testMethod void testDuplicates() {
		List<Opportunity> opportunities = new List<Opportunity>();

		opportunities.add(new Opportunity(name='NAME1'));
		opportunities.add(new Opportunity(name='NAME2'));
		opportunities.add(new Opportunity(name='NAME2'));
		opportunities.add(new Opportunity(name='NAME2'));


		Map<Object, Set<SObject>> oppsByName = Collector.collect(opportunities).byField(Opportunity.name).withDuplicates();

		System.assert(oppsByName.get('NAME1') != null);
		System.assert(oppsByName.get('NAME2') != null);

		System.assert(oppsByName.get('NAME1').contains(opportunities.get(0)));
		System.assert(oppsByName.get('NAME2').contains(opportunities.get(1)));
		System.assert(oppsByName.get('NAME2').contains(opportunities.get(2)));
		System.assert(oppsByName.get('NAME2').contains(opportunities.get(3)));

	}

	public static testMethod void testNotSupported() {
		List<Opportunity> opportunities = new List<Opportunity>();

		try {
			Map<Object, Set<SObject>> oppsByName = Collector.collect(opportunities).byField(Opportunity.name).byField(Opportunity.Id).withDuplicates();
		} catch (Exception e) {
			return;
		}

		System.assert(false, 'No exception was thrown');
	}


	public static testMethod void testNullCollection() {
		List<Opportunity> opportunities = null;

		try {
			Map<Object, Set<SObject>> oppsByName = Collector.collect(opportunities).byField(Opportunity.name).withDuplicates();
		} catch (Exception e) {
			return;
		}

		System.assert(false, 'No exception was thrown');
	}

}